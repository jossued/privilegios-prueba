# Prueba de Privilegios

Prueba de privilegios en Gitlab

# Índice
* [Pruebas Guest](#guest-inicio)
* [Pruebas Reporter](#reporter-inicio)
* [Pruebas Developer](#developer-inicio)
* [Pruebas Maintainer](#maintainer-inicio)

# Guest [(Inicio)](#índice)

### Issues
Se puede crear issues y etiquetar
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/7ac17515af8a1654d17050f581e065aa/image.png)

### Milestone
No me deja crear ni modificar los milestones
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/b5df25ae5f366f16abc9a8a88676ea66/image.png)

### Labels
No se puede crear ni modificar los labels
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/4cb666167c9eaa3d177645ed3f5b6a90/image.png)

### Ramas
No se puede crear más branches
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/8d8febd97ce926709fac5e852bcef823/image.png)

### Merge
Se intenta crear un archivo:

Gitlab nos dice que se debe hacer un fork para poder hacer cambios en el proyecto.
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/af089083eac4287e80a4c4663e63f1da/image.png)

Creación del archivo:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/ae1395420dc586994a7da1460c1e6fa6/image.png)

Se intenta crear el Pull request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/26b12f0a53487e2bde55290dd0a55c55/image.png)

No se puede aceptar el Merge:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/9962fe15f6f12b92ad8d6c696846d276/image.png)

### Otros.

No se puede añadir a otros miembros al proyecto.
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/c39294cf8a5053f1da07b6b5e2df3b2a/image.png)

# Reporter [(Inicio)](#índice)

### Issues
Se puede crear issues y etiquetar
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/4eb704f83db5fc6c0efa728456eb1542/image.png)

También se puede editar los Issues
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/1b28f0f3368d6901cfd404d502b5a41b/image.png)

### Milestone
No me deja crear ni modificar los milestones
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/f006448480003fb3d59c3d753ce4f7b8/image.png)

### Labels
Puedo crear los labels:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/941007e5bd5de1ff5bb1e62d67285d7f/image.png)

También me permite editarlos:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/62a9a9670421b86ee36ca243773437ed/image.png)

### Ramas
No se puede crear más branches ni modificarlas desde Gitlab
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/d80aa542628002bc888c72312bc3b7a0/image.png)

### Merge

Se intenta crear un archivo:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/a2c59fb1f9f67640e8600307b6fbbf3c/image.png)

Se intenta crear el Pull request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/7a4c9b6b9c2592f913c8055f76b6f92d/image.png)

No se puede aceptar el Merge:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/0e6b127bb392c49523bc281376f6be62/image.png)

### Otros.

No se puede añadir a otros miembros al proyecto.
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/b63989d29bdf93f3bbeb4a725282efe6/image.png)

No se puede crear una wiki:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/84860686e565ccddafa8145578faca9d/image.png)

# Developer [(Inicio)](#índice)

### Issues
Se puede crear issues y etiquetar
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/a36ed1c43c861b73611463e788b2a443/image.png)

También se pueden editar los Issues
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/e7189f6addc31ed7b7e6fea3ac9da578/image.png)

Adicionalmente se puede hacer un tracking de los issues como developer:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/6dc42640d0af2b689f5db3a2a6523672/image.png)

### Milestone
Permite crear nuevos Milestones en el proyecto
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/3dec1f4eda6ee95f06b976ae7b9c91be/image.png)

Adicionalmente me permite cerrarlo y editarlo
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/f98ad5b64e4e1533a2513e9a1f1b79d1/image.png)

### Labels
Puedo crear los labels:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/505d83a90af2576f64e3df2c7e4e0c25/image.png)

También me permite editarlos:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/d93ba3ab725597ed987027096ba8a7ad/image.png)

### Ramas
Puedo crear nuevas branches o ramas:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/6ab9fbf15d318b981d90ec0d02080b92/image.png)

Adicionalmente puedo modificar las ramas:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/5f2d46624299b1de987c9455a60e9615/image.png)

### Merge

Se intenta crear un archivo directamente en el repositorio:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/27a4710719d1ecc930b2891e0eb62ed4/image.png)

El archivo se crea satisfactoriamente:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/79867a7bde68a7e6597d2ea9acbb02e3/image.png)

Se puede crear el Merge Request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/487a176e226eb1f83f3f4bd988c5fc7b/image.png)

No se puede aceptar el Merge Request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/866f540eb099603b674d949912867c54/image.png)

### Otros.

No se puede añadir a otros miembros al proyecto.
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/988a4901fa4b068217686f1c14d125f1/image.png)

Se puede crear una wiki:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/fe801d57aa7c4d6bce35182dc95ea1dd/image.png)

# Maintainer [(Inicio)](#índice)

### Issues
Se puede crear issues y asignar
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/010c4697a6b8ff2177f9c56579cdb6d9/image.png)

También se pueden editar los Issues
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/5dc8398085274dbfd35b74c902ef3577/image.png)

Adicionalmente se puede hacer un tracking de los issues 
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/3275ef7161f158c3f71a613f47c7cd4d/image.png)

### Milestone
Permite crear nuevos Milestones en el proyecto
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/e3e412db23c0e3d65cf65d35eac96ad0/image.png)

Adicionalmente me permite cerrarlo y editarlo
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/77fedeba60c06e0748397da27bd996dd/image.png)

### Labels
Puedo crear los labels:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/1a5b1ffae8ade517673079d336bc9620/image.png)

También me permite editarlos:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/75cda6b1af983245ed5c3eaf6307640f/image.png)

### Ramas
Puedo crear nuevas branches o ramas:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/47b41146018b228ad87de0e3767eaac1/image.png)

Adicionalmente puedo modificar las ramas:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/6fb79736ea552a31d8dcc46bbb493a3e/image.png)

### Merge

Se intenta crear un archivo directamente en el repositorio:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/e76f92fcc3dc92eb9a49825cdff3be54/image.png)

El archivo se crea satisfactoriamente:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/807017ff36d7ae05322f9bb510c9ef55/image.png)

Se puede crear el Merge Request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/bdcb537a53570742c9110c11fc77483f/image.png)

Se puede aceptar el Merge Request:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/cf1041e2c06553e6a9c4db7b4aa8e3af/image.png)
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/68925137d075c31e85df88ef5a22f53e/image.png)

### Otros.

Se puede añadir a otros miembros al proyecto.
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/9cdaea08a0fcaaff488e363aecbce9fc/image.png)

Se puede crear una wiki:
![image](https://gitlab.com/AnalyCh/prueba-roles/uploads/73e58b8f08759ea532e50f947bbf8614/image.png)